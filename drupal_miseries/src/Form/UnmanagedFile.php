<?php

namespace Drupal\drupal_miseries\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class UnmanagedFile extends FormBase {
  
  
  public function getFormId() {
    return 'UnmanagedFile_Form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
        '#type' => 'file',
        '#title' => $this->t('Upload File'),
        '#description' => $this->t('Select a file and upload'),
    ];
    
    $form['actions'] = [
        '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Upload'),
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    dpm($form_state->getValues());
  }
}