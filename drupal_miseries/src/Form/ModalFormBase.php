<?php

namespace Drupal\drupal_miseries\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormState;

class ModalFormBase extends FormBase {
  
  protected $form_builder;
  
  public function __construct(FormBuilder $form_builder) {
    $this->form_builder = $form_builder;
  }
  
  public static function create(ContainerInterface $container) {
    return new static (
        $container->get('form_builder')
    );
  }
  
  public function getFormId() {
    return 'drupal_miseries_modalformbase';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    
    $form['textfield'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Value'),
        '#description' => $this->t('Show value retorned by modalform'),
        '#default_value' => $form_state->getValue('textfield'),
    ];
    
    $form['open_modal'] = [
        '#type' => 'button',
        '#name' => 'openbutton',
        '#value' => $this->t('Open Modal Form'),
        '#ajax' => [
          'callback' => [$this, 'openModalForm'],
          'event' => 'click',
        ],
    ];
    
    $form['actions'] = [
        '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
    ];
    
    $form_state->setRebuild();
    
    return $form;
  }
  
  public function openModalForm(array &$form, FormStateInterface $form_state) {
    dpm('Inicio ajax: '.$form_state->getValue('textfield'));
    $response = new AjaxResponse();
    $modal_form_state = new FormState();
    $modal_form_state->setValue('textfield_modal', 'Probando');
    $modal_form = $this->form_builder->buildForm('Drupal\drupal_miseries\Form\ModalForm', $modal_form_state);
    $response->addCommand(new OpenModalDialogCommand($this->t('Modal Form'), $modal_form));
    dpm('Fin ajax: '.$form_state->getValue('textfield'));
    return $response;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message($this->t('Form submitted'));
  }

}