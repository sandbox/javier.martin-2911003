<?php

namespace Drupal\drupal_miseries\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Checkboxes;

class CheckboxesAjax extends FormBase {

    protected $values = [
        'select' => [
            '1' => 'Value1',
            '2' => 'Value2',
        ],
        // Options when select 1 is selected
        'checkboxes_v1' => [
            '1' => 'checkbox1',
            '2' => 'checkbox2',
        ],
        // Options when select 2 is selected
        'checkboxes_v2' => [
            '3' => 'checkbox3',
            '4' => 'checkbox4',
        ],
        // Default values for checkboxes
        'checkboxes_df' => [
            // Default value when checkboxes_v1 is showed
            '1' => ['1' => '1', '2' => '0'],
            // Default value when checkboxes_v2 is showed
            '2' => ['3' => '0', '4' => '1'],
        ],
    ];

    public function getFormId() {
        return 'drupal_miseries_checkboxesajax';
    }
    
    public function selectAjaxCallback(array &$form, FormStateInterface $form_state) {
      $select = $form_state->getValue('select');
      
      foreach ($this->values['checkboxes_df'][$select] as $key => $value) {
        $form['container']['checkboxes'][$key]['#checked'] = $value;
      }
      return $form['container'];
    }
    
    public function checkboxesAjaxCallback(array &$form, FormStateInterface $form_state) {
      return $form['status'];
    }
    
    public function buildForm(array $form, FormStateInterface $form_state) {
      $element = $form_state->getTriggeringElement();
      
      $message = '';
      if (!empty($element) && $element['#type'] == 'checkbox') {
        $message = 
          '<p>'.
          $this->t('Select : @select @checkbox clicked.',
              [
                  '@select' => $form_state->getValue('select'),
                  '@checkbox' => $element['#title'],
              ]).
          '</p>';
      }
      
      $form['select'] = [
        '#type' => 'select',
        '#title' => $this->t('Select form control'),
        '#options' => $this->values['select'],
        '#default_value' => key($this->values['select']),
        '#ajax' => [
            'callback' => [$this, 'selectAjaxCallback'],
            'wrapper' => 'checkboxes_wrapper',
        ],
      ];

      $form['container'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'checkboxes_wrapper'],
      ];
        
      $form['container']['checkboxes'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Checkboxes form control from select @select',
          [ '@select' => empty($form_state->getValues()) ? 
              $this->values['select'][key($this->values['select'])] : $form_state->getValue('select') ]),
        '#options' => empty($form_state->getValues()) ?
          $this->values['checkboxes_v1'] :
          $this->values['checkboxes_v'.$form_state->getValue('select')],
        '#default_value' => empty($form_state->getValues()) ?
          $this->values['checkboxes_df']['1'] :
          $this->values['checkboxes_df'][$form_state->getValue('select')],
        '#ajax' => [
            'callback' => [$this, 'checkboxesAjaxCallback'],
            'wrapper' => 'status',
        ],
      ];
        
      $form['status'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'status'],
      ];
      
      $form['status']['message'] = [
        '#access' => empty($message) ? FALSE : TRUE,
        '#markup' => $message,
      ];
      
      return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {

    }
}
