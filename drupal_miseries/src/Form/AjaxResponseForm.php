<?php

namespace Drupal\drupal_miseries\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

class AjaxResponseForm extends FormBase {
  protected $ts_values = [
    'ts1' => [
      'values1' => [
        'id' => 1,
        'description' => 'Valor1',
        'status' => '1',
      ],
      'values2' => [
          'id' => 2,
          'description' => 'Valor2',
          'status' => 0,
      ],
    ],
  ];
  
  public function getFormId() {
    return 'AjaxResponseForm';
  }
  
  public function ajaxTableSelect(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    if (isset($element) && $element['#type'] == 'checkbox') {
      if ($element['#return_value'] == $element['#default_value']) {
        $check_value = $element['#return_value'];
        $textfield_ts = $this->ts_values['ts1'][$check_value]['description'];
        $checkbox_ts = $this->ts_values['ts1'][$check_value]['status'];
        $form['ts_textfield']['textfield']['#value'] = $textfield_ts;
        $form['ts_checkbox']['checkbox']['#checked'] = ($checkbox_ts == 1) ? 'checked' : null;
      } else {
        $form['ts_textfield']['textfield']['#value'] = null;
        $form['ts_checkbox']['checkbox']['#checked'] = null;
      }
    }
    
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#ts-textfield', render($form['ts_textfield'])));
    $response->addCommand(new ReplaceCommand('#ts-checkbox', render($form['ts_checkbox'])));
    
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['ts_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'ts-container'],
    ];
    
    $header = [
      'id' => $this->t('ID'),
      'description' => $this->t('Description'),
      'status' => $this->t('Status'),
    ];
    
    $form['tableselect'] = [
      '#type' => 'tableselect',
      '#description' => $this->t('Test Tableselect'),
      '#header' => $header,
      '#options' => $this->ts_values['ts1'],
      '#empty' => $this->t('No values'),
      '#ajax' => [
        'callback' => [$this, 'ajaxTableSelect'],
      ],
    ];
    
    $form['ts_textfield'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'ts-textfield'],
    ];
    
    $form['ts_textfield']['textfield'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Value fill by ajax'),
      '#title' => $this->t('Ajax Textfield'),
    ];
    
    $form['ts_checkbox'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'ts-checkbox'],
    ];
    
    $form['ts_checkbox']['checkbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ajax Checkbox'),
      '#description' => $this->t('Value fill by ajax'),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['action']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    dpm($form_state->getValue('tableselect'));
    dpm($form_state->getValue('textfield'));
    dpm($form_state->getValue('checkbox'));
  }
}