<?php

namespace Drupal\drupal_miseries\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ModalForm extends FormBase {
  
  public function getFormId() {
    return 'drupal_miseries_modalform';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['#prefix'] = '<div id="test-modal-form">';
    $form['#suffix'] = '</div>';
    
    $form['textfield_modal'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Intro a value'),
      '#title' => $this->t('Modal Textfield'),
      '#default_value' => $form_state->getValue('textfield_modal'),
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('textfield', $form_state->getValue('textfield_modal'));
    drupal_set_message($this->t('Form submitted'));
  }
}