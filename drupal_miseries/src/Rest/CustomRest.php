<?php 

namespace Drupal\drupal_miseries\Rest;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

class CustomRest extends ControllerBase {
  protected $entityTypeManager;
  protected $serializer;
  protected $logger;
  
  /**
   * Construct implementation.
   * @param EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, SerializerInterface $serializer,
    LoggerChannelFactoryInterface $logger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->serializer = $serializer;
    $this->logger = $logger->get('drupal_miseries - customrest');
  }
  
  /**
   * Create implementation.
   * @param ContainerInterface $container
   * @return \Drupal\drupal_miseries\Rest\CustomRest
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('entity_type.manager'),
      $container->get('serializer'),
      $container->get('logger.factory')
    );
  }
  
  /**
   * Return 10 last nodes created in a Json.
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getLatestNodes() {
    $response_array = array();
    
    // Get node storage from entitytype.manager service.
    $node_storage = $this->entityTypeManager->getStorage('node');
    
    // Exe a query that return 10 last nodes created.
    $query = $node_storage->getQuery()
    ->condition('type', 'article')
    ->condition('status', 1)
    ->sort('changed', 'DESC')
    ->range(0, 10)
    ->execute();
    
    // Check if query not return value
    if (!empty($query)) {
      // Load all returned nodes.
      $nodes = $node_storage->loadMultiple($query);
      foreach ($nodes as $node) {
        // Get node title and add to return array.
        $response_array[] = [
          'title' => $node->get('title')->value,
        ];
      }
    } else {
      $response_array[] = [
        'message' => $this->t('No nodes stored.'),
      ];
    }
    
    // Transform array in a Json response.
    $response = new JsonResponse($response_array);
    
    return $response;
  }
  
  /**
   * Create node and returned it serialized as json
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function createNode(Request $request) {
    $storage = $this->entityTypeManager->getStorage('node');
    
    // Get data value from request
    $data = $request->request->get('data');
    
    // Transform json from request to associative array
    $values = json_decode($data, true);
    
    // Create node entity
    $node = $storage->create([
      'type' => $values['node']['type'],
      'title' => $values['node']['title'],
      'body' => [
        'summary' => $values['node']['body']['summary'],
        'value' => $values['node']['body']['value'],
        'format' => $values['node']['body']['format'],
      ],
    ]);
    
    // Save entity
    $node->save();
    
    // Encode node as json 
    $node_encoded = $this->serializer->serialize($node, 'json', ['plugin_id' => 'entity']);
    
    // Build response with node serialized
    $response = new Response($node_encoded);
    
    return $response;
  }
  
  /**
   * Update node title.
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function updateNode(Request $request) {
    $storage = $this->entityTypeManager->getStorage('node');
    
    // Get data value from request
    $data = $request->request->get('data');
    
    // Transform json from request to associative array
    $values = json_decode($data, TRUE);
    
    // Try to load node
    $node = $storage->load($values['nid']);
    
    // If node exist update title and return OK message
    if (!empty($node)) {
      // Set new title and save node
      $node->setTitle($values['title']);
      $node->save();
      
      //Create response with OK message
      $response = new JsonResponse([
        'response' => 'Node updated.'
      ]);
    } else {
      // If node not exists return error message
      $response = new JsonResponse([
        'response' => 'Node not exists.',
      ], 404);
    }
    
    // Return response
    return $response;
  }
  
  /**
   * Delete node.
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function deleteNode(Request $request) {
    $storage = $this->entityTypeManager->getStorage('node');
    
    // Get data value from request
    $data = $request->request->get('data');
    
    // Transform json from request to associative array
    $values = json_decode($data, TRUE);
    
    // Try to load node
    $node = $storage->load($values['nid']);
    
    if (!empty($node)) {
      // Delete node.
      $node->delete();
      
      //Create response with OK message
      $response = new JsonResponse([
        'response' => 'Node deleted.'
      ]);
    } else {
      $response = new JsonResponse([
        'response' => 'Node not exists.',
      ], 404);
    }
    
    return $response;
  }
}