<?php 

namespace Drupal\drupal_miseries\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\AppendCommand;

class HideShowAjaxButton extends FormBase {
  
  protected $values = [
    '0' => [
      'textfield_1' => 'Texto 1',
      'checkbox_1' => TRUE,
    ],
    '1' => [
      'textfield_1' => 'Texto 2',
      'checkbox_1' => FALSE,
    ],
  ];
  
  public function getFormId() {
    return 'HideShowAjaxButtonForm';
  }
  
  public function ajaxButton1 (array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    
    if ($form_state->get('showfieldset1')) {
      $response->addCommand(new InvokeCommand('#fieldset-1', 'show'));
    } else {
      $response->addCommand(new InvokeCommand('#fieldset-1', 'hide'));
    }
    
    $response->addCommand(new AppendCommand('#status', '<p>Ajaxbutton1 showfieldset1: '. $form_state->get('showfieldset1') .'</p>'));
    $response->addCommand(new AppendCommand('#status', '<p>Textfield 1: '. $form_state->getValue('textfield_1') .'</p>'));
    
    return $response;
  }
  
  public function ajaxButton2 (array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    
    $form['fieldset_1']['textfield_1']['#value'] = $this->values['1']['textfield_1'];
    $form['fieldset_1']['checkbox_1']['#checked'] = ($this->values['1']['checkbox_1']) ? 'checked' : '';
    
    $response->addCommand(new ReplaceCommand('#fieldset-1', render($form['fieldset_1'])));
    
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) { 
    
    $element = $form_state->getTriggeringElement();
    if (empty($element)) {
      $form_state->set('showfieldset1', TRUE);
    } else if (!empty($element) && $element['#name'] == 'ajaxbutton1') {
      $showfieldset1 = $form_state->get('showfieldset1');
      $form_state->set('showfieldset1', !$showfieldset1);
    }
    
    $form['fieldset_1'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fieldset container 1'),
      '#attributes' => [
        'id' => 'fieldset-1',
      ],
    ];
    
    $form['fieldset_1']['textfield_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Textfield 1'),
      '#description' => $this->t('Example textfield 1'),
      '#default_value' => $this->values['0']['textfield_1'],
     ];
    
    $form['fieldset_1']['checkbox_1'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Checkbox 1'),
      '#default_value' => $this->values['0']['checkbox_1'],
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['button_1'] = [
      '#type' => 'button',
      '#name' => 'ajaxbutton1',
      '#value' => $this->t('Show/Hide Fieldset'),
      '#ajax' => [
        'callback' => [$this, 'ajaxButton1'],
      ],
    ];
    
    $form['actions']['button_2'] = [
      '#type' => 'button',
      '#name' => 'ajaxbutton2',
      '#value' => $this->t('Change Values'),
      '#ajax' => [
        'callback' => [$this, 'ajaxButton2'],
      ],
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    $form['status'] = [
      '#markup' => '
        <div id="status"></div>',
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { 
    drupal_set_message($form_state->getValue('textfield_1'));
    drupal_set_message($form_state->getValue('checkbox_1'));
  }
}