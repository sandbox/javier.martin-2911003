<?php 

namespace Drupal\drupal_miseries\Form;

use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\FileInterface;
use Drupal\Core\File\FileSystemInterface;
use Egulias\EmailValidator\EmailValidatorInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

class ImportUsers extends FormBase {
  
  protected $fileUsage;
  protected $current_user;
  protected $entityTypeManager;
  protected $fileSystem;
  protected $emailValidator;
  
  public function __construct(DatabaseFileUsageBackend $fileUsage, AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entityTypeManager, FileSystemInterface $fileSystem,
    EmailValidatorInterface $emailValidator) {
    $this->fileUsage = $fileUsage;
    $this->current_user = $current_user;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->emailValidator = $emailValidator;
  }
  
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('file.usage'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('email.validator')
    );
  }
  
  public function getFormId() {
    return 'drupalmiseriesImportUsers';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#markup' => $this->t('Form designed to show how to import users.'),
    ];
    
    $form['files'] =  [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload file'),
      '#upload_location' => 'private://drupal_miseries',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#required' => TRUE,
      '#weight' => 1,
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 2,
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    $header = [
      'filename' => $this->t('File name'),
    ];
    
    $form['files_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('Sorry, no uploaded files.'),
      '#caption' => $this->t('File List'),
      '#weight' => 3,
    ];
    
    
    foreach ($this->getFiles() as $key => $value) {
      $form['files_table'][$key] = $value;
    }
    
    return $form;
  }
  
  protected function getFiles() {
    $storage_files = $this->entityTypeManager->getStorage('file');
    $files = $storage_files->loadMultiple();
    $list_files = array();
    
    foreach ($files as $id => $file) {
      $fileusage = $this->fileUsage->listUsage($file);
      if ( array_key_exists('drupal_miseries', $fileusage) ) {
        $uri = $file->get('uri')->value;
        $name = $file->get('filename')->value;
        $url_file = file_create_url($uri);
        
        $url = Url::fromUri($url_file);
        $link = Link::fromTextAndUrl($name, $url);
        
        $list_files[$id]['filename'] = $link->toRenderable();
      }
    }
    
    return $list_files;
  }
  
  protected function processFile(FileInterface $file) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    
    $uri = $file->get('uri')->value;
    $path = $this->fileSystem->realpath($uri);
    
    $linesprocess = 0;
    $lineerrors = 0;
    $fp = fopen($path, 'r');
    while ( !feof($fp) ) {
      $line = fgetcsv($fp, null, ';');
      
      /* Check if number of fields is ok and email is correct. */
      if ( (sizeof($line) != 3) || (!$this->emailValidator->isValid($line[2])) ) {
        $lineerrors++;
        continue;
      }
      
      $users = $user_storage->loadByProperties(['name' => $line[0]]);
      if ( !empty($users) ) {
        $lineerrors++;
        continue;
      }

      /* Mail ok, login ok -> Insert new user */
      $user_values = [
        'name' => $line[0],
        'pass' => $line[1],
        'mail' => $line[2],
        'status' => 1,
      ];
      
      $user = $user_storage->create($user_values);
      $user->save();
      
      drupal_set_message($this->t('New user inserted.'));
      $linesprocess++;
    }
    
    drupal_set_message( $this->t('Total users inserted: ' . $linesprocess . ' Total errors line: ' . $lineerrors) );
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) { 
    $storage = $this->entityTypeManager->getStorage('file');
    
    foreach ($form_state->getValue('files') as $fid) {
      $file = $storage->load($fid);
      $this->processFile($file);
      $this->fileUsage->add($file, 'drupal_miseries', 'user', $this->current_user->id(), 1);
      drupal_set_message( $this->t('File uploaded: '. $file->get('filename')->value) );
    }
  }
}